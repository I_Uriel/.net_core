﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Clientes_Core.Models;
using Microsoft.EntityFrameworkCore;

namespace Clientes_Core.Controllers
{
    public class ClienteController : Controller
    {
        private readonly ApplicationDbContext _db;

        public ClienteController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {     //Aquí va el nombre de la base de datos
            var displaydata = _db.Clientes.ToList();
            return View(displaydata);
        }

        [HttpGet]

        public async Task<IActionResult> Index(string cliSearch)
        {
            ViewData["GetClientesDetails"] = cliSearch;
            var cliquery = from x in _db.Clientes select x;
            if(!String.IsNullOrEmpty(cliSearch))
            {
                cliquery = cliquery.Where(x => x.Cli_nombre.Contains(cliSearch) ||
                x.Correo.Contains(cliSearch));
            }
            return View(await cliquery.AsNoTracking().ToListAsync());
        }


        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Cliente nCli)
        {
            if (ModelState.IsValid)
            {
                _db.Add(nCli);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(nCli);
        }

        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getClientesDetail = await _db.Clientes.FindAsync(id);
            return View(getClientesDetail);     //lo que hay en var tiene que coincidir con lo que esta dentro del parentesis
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getClientesDetail = await _db.Clientes.FindAsync(id);
            return View(getClientesDetail);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Cliente oldCli)
        {
            if(ModelState.IsValid)
            {
                _db.Update(oldCli);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldCli);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getClientesDetail = await _db.Clientes.FindAsync(id);
            return View(getClientesDetail);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var getClidetail = await _db.Clientes.FindAsync(id);
            _db.Clientes.Remove(getClidetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}
