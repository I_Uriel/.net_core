﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Clientes_Core.Models
{
    public class Cliente
    {
        [Key]
        public int Cli_id { get; set; }

        [Required(ErrorMessage = "Ingresa el nombre del cliente")]
        [Display(Name = "Nombre del Cliente ")]
        public string Cli_nombre { get; set; }

        [Required(ErrorMessage = "Credito")]
        [Display(Name = "Credito")]
        public int Credito { get; set; }

        [Required(ErrorMessage = "Edad del Cliente")]
        [Display(Name = "Edad")]
        [Range(18, 85)]
        public int Edad { get; set; }

        [Required(ErrorMessage = "Correo del Cliente")]
        [Display(Name ="Correo")]
        [EmailAddress(ErrorMessage = "Ingrese una dirección de correo valida")]
        public string Correo { get; set; }

        
    }
}
