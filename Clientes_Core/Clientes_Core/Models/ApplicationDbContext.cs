﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Clientes_Core.Models
{
    public class ApplicationDbContext : DbContext
    {   //aquí va el constructor
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }   //el primer Cliente es nombre de la tabla el segundo es nommbre de la base.
        public DbSet<Cliente> Clientes { get; set; }
    }
    

}
