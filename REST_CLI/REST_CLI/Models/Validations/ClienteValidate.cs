﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace REST_CLI.Models
{
    [MetadataType(typeof(Clientes.MetaData))]
    public partial class Clientes 
    {
        sealed class MetaData{
            [Key]
            public int Cli_id;

            [Required(ErrorMessage = "Ingresa el nombre del Cliente")]
            public string Cli_nombre;

            [Required]
            [EmailAddress(ErrorMessage = "Ingresa un correo valido")]
            public string Correo;

            [Required]
            [Range(18,85,ErrorMessage = "Edad entre 18 y 85")]
            public Nullable<int> Edad;

            [Required(ErrorMessage = "Ingresa credito")]
            public Nullable<int> Credito;

        }
    }
}