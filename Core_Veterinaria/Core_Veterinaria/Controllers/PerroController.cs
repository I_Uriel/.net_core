﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core_Veterinaria.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Core_Veterinaria.Controllers
{
    public class PerroController : Controller
    {
        private readonly ApplicationDbContext _db;

        public PerroController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            var displaydata = _db.Perros.ToList();
            return View(displaydata);
        }

        [HttpGet]
        public async Task<IActionResult> Index(string perroSearch)
        {
            ViewData["GetPerroDetails"] = perroSearch;
            var perroquery = from x in _db.Perros select x;
            if (!String.IsNullOrEmpty(perroSearch))
            {
                perroquery = perroquery.Where(x => x.Nombre.Contains(perroSearch)||
                x.Raza.Contains(perroSearch));
            }
            return View(await perroquery.AsNoTracking().ToListAsync());

        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Perro nPerro)
        {
            if (ModelState.IsValid)
            {
                _db.Add(nPerro);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(nPerro);
        }

        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index)");
            }
            var getPerroDetail = await _db.Perros.FindAsync(id);
            return View(getPerroDetail);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index)");
            }

            var getPerroDetail = await _db.Perros.FindAsync(id);
            return View(getPerroDetail);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Perro oldPerro)
        {
            if (ModelState.IsValid)
            {
                _db.Update(oldPerro);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldPerro);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index)");
            }

            var getPerroDetail = await _db.Perros.FindAsync(id);
            return View(getPerroDetail);
        }
         
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var getPerrodetail = await _db.Perros.FindAsync(id);
            _db.Perros.Remove(getPerrodetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}
