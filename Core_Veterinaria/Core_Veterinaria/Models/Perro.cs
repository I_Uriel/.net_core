﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Core_Veterinaria.Models
{
    public class Perro
    {
        [Key]
        public int P_id { get; set; }

        [Required(ErrorMessage = "Ingresa el nombre del perro")]
        [Display(Name ="Nombre del perro")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Ingresa la raza")]
        [Display(Name ="Raza")]
        public string Raza { get; set; }

        [Required(ErrorMessage = "Ingresa la vacuna")]
        [Display(Name = "Vacuna")]
        public string Vacunas { get; set; }

        [Required(ErrorMessage = "Ingresa el sexo")]
        [Display(Name = "Sexo")]
        public string Sexo { get; set; }

        [Required(ErrorMessage = "Ingresa la edad")]
        [Display(Name = "Edad")]
        [Range(1, 22)]
        public int Edad { get; set; }


    }
}
