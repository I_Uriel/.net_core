﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace REST_EMPLOYEE.Models
{
    [MetadataType(typeof(Employee.MetaData))]
    public partial class Employee
    {
        sealed class MetaData{
            [Key]
            public int Empid;

            [Required(ErrorMessage = "Ingresa el nombre del empleado")]
            public string Empname;

            [Required]
            [EmailAddress(ErrorMessage = "Ingresa email valido")]
            public string Email;

            [Required]
            [Range(20,50, ErrorMessage = "Ingresa edad entre 20 y 50")]
            public Nullable<int> Age;

            [Required(ErrorMessage = "Ingresa el salario")]
            public Nullable <int> Salary;




        }
    }
}