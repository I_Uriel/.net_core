﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Rest_Perros.Models;

namespace Rest_Perros.Controllers
{
    public class PerrosController : ApiController
    {
        private VETEntities2 db = new VETEntities2();

        // GET: api/Perros
        public IQueryable<Perros> GetPerros()
        {
            return db.Perros;
        }

        // GET: api/Perros/5
        [ResponseType(typeof(Perros))]
        public IHttpActionResult GetPerros(int id)
        {
            Perros perros = db.Perros.Find(id);
            if (perros == null)
            {
                return NotFound();
            }

            return Ok(perros);
        }

        // PUT: api/Perros/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPerros(int id, Perros perros)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != perros.P_id)
            {
                return BadRequest();
            }

            db.Entry(perros).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PerrosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Perros
        [ResponseType(typeof(Perros))]
        public IHttpActionResult PostPerros(Perros perros)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Perros.Add(perros);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = perros.P_id }, perros);
        }

        // DELETE: api/Perros/5
        [ResponseType(typeof(Perros))]
        public IHttpActionResult DeletePerros(int id)
        {
            Perros perros = db.Perros.Find(id);
            if (perros == null)
            {
                return NotFound();
            }

            db.Perros.Remove(perros);
            db.SaveChanges();

            return Ok(perros);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PerrosExists(int id)
        {
            return db.Perros.Count(e => e.P_id == id) > 0;
        }
    }
}