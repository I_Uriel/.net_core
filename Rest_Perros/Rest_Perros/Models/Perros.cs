//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Rest_Perros.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Perros
    {
        public int P_id { get; set; }
        public string Nombre { get; set; }
        public string Raza { get; set; }
        public string Vacunas { get; set; }
        public string Sexo { get; set; }
        public Nullable<int> Edad { get; set; }
    }
}
